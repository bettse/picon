#!/usr/bin/python
#
# YAPTB Bluetooth keyboard emulator DBUS Service
#
# Adapted from
# www.linuxuser.co.uk/tutorials/emulate-bluetooth-keyboard-with-the-raspberry-pi

from __future__ import absolute_import, print_function

from optparse import OptionParser, make_option
import os
import sys
import uuid
import dbus
import dbus.service
import dbus.mainloop.glib
import time
import bluetooth
import struct
import random
from bluetooth import *
import threading
import crc8

import gtk, gobject
from dbus.mainloop.glib import DBusGMainLoop


#define a bluez 5 profile object for our keyboard
class BTKbBluezProfile(dbus.service.Object):
    fd = -1

    @dbus.service.method("org.bluez.Profile1", in_signature="", out_signature="")
    def Release(self):
        print("Release")
        mainloop.quit()

    @dbus.service.method("org.bluez.Profile1", in_signature="", out_signature="")
    def Cancel(self):
        print("Cancel")

    @dbus.service.method("org.bluez.Profile1", in_signature="oha{sv}", out_signature="")
    def NewConnection(self, path, fd, properties):
        self.fd = fd.take()
        print("NewConnection(%s, %d)" % (path, self.fd))
        for key in properties.keys():
            if key == "Version" or key == "Features":
                print("  %s = 0x%04x" % (key, properties[key]))
            else:
                print("  %s = %s" % (key, properties[key]))


    @dbus.service.method("org.bluez.Profile1", in_signature="o", out_signature="")
    def RequestDisconnection(self, path):
        print("RequestDisconnection(%s)" % (path))

        if (self.fd > 0):
            os.close(self.fd)
            self.fd = -1

    def __init__(self, bus, path):
        dbus.service.Object.__init__(self, bus, path)

#
#create a bluetooth device to emulate a HID keyboard,
# advertize a SDP record using our bluez profile class
#
class BTKbDevice():
    #change these constants
    MY_ADDRESS="E0:76:D0:DD:AA:99"
    MY_DEV_NAME="Joy-Con (R)"

    #define some constants
    P_CTRL =17  #Service port - must match port configured in SDP record
    P_INTR =19  #Service port - must match port configured in SDP record#Interrrupt port
    PROFILE_DBUS_PATH="/bluez/yaptb/btkb_profile" #dbus path of the bluez profile we will create
    SDP_RECORD_PATH = sys.path[0] + "/joycon-hid.xml" #file path of the sdp record to laod
    UUID="00001124-0000-1000-8000-00805f9b34fb"

    OutputReportSize = 49
    InputReportSize = 362
    neutralPosition = bytes('\xa1\x3f\x00\x00\x08\x00\x80\x00\x80\x00\x80\x00\x80')

    imu = False
    reportMode = 0x3F
    sequence = 0x00
    wait = 0.1 # seconds to sleep before looking for incoming messages
    buttons = [0] * 0x3 # 1 byte right, 1 byte shared, 1 byte left
    mcuConfig = 0x00

    def __init__(self):
        print("Setting up BT device")

        self.init_bt_device()
        self.init_bluez_profile(BTKbDevice.UUID, BTKbDevice.SDP_RECORD_PATH, BTKbDevice.PROFILE_DBUS_PATH)
        #self.init_bluez_profile("00001200-0000-1000-8000-00805f9b34fb", sys.path[0] + "/joycon-pnp.xml", "/bluez/yaptb/joycon_pnp")

    #configure the bluetooth hardware device
    def init_bt_device(self):
        print("Configuring for name " + BTKbDevice.MY_DEV_NAME)

        #set the device class to a keybord and set the name
        # os.system("hciconfig hci0 class 0x002508")
        os.system("hciconfig hci0 name '" + BTKbDevice.MY_DEV_NAME + "'")

        #make the device discoverable
        os.system("hciconfig hci0 piscan")


    #set up a bluez profile to advertise device capabilities from a loaded service record
    def init_bluez_profile(self, uuid, sdp_path, dbus_path):
        print("Configuring Bluez Profile")

        #setup profile options
        service_record=self.read_sdp_service_record(sdp_path)

        opts = {
                "ServiceRecord":service_record,
                "Role":"server",
                "RequireAuthentication":False,
                "RequireAuthorization":False
                }

        #retrieve a proxy for the bluez profile interface
        bus = dbus.SystemBus()
        manager = dbus.Interface(bus.get_object("org.bluez","/org/bluez"), "org.bluez.ProfileManager1")

        profile = BTKbBluezProfile(bus, dbus_path)

        manager.RegisterProfile(dbus_path, uuid, opts)

        print("Profile registered ")


    #read and return an sdp record from a file
    def read_sdp_service_record(self, path):
        print("Reading service record")

        try:
            fh = open(path, "r")
        except:
            sys.exit("Could not open the sdp record. Exiting...")

        return fh.read()


    #listen for incoming client connections
    #ideally this would be handled by the Bluez 5 profile
    #but that didn't seem to work
    def listen(self):
        print("Waiting for connections")
        self.scontrol=BluetoothSocket(L2CAP)
        self.sinterrupt=BluetoothSocket(L2CAP)

        #bind these sockets to a port - port zero to select next available
        self.scontrol.bind((self.MY_ADDRESS,self.P_CTRL))
        self.sinterrupt.bind((self.MY_ADDRESS,self.P_INTR ))

        #Start listening on the server sockets
        self.scontrol.listen(1) # Limit of 1 connection
        self.sinterrupt.listen(1)

        self.ccontrol, cinfo = self.scontrol.accept()
        print ("Got a connection on the control channel from " + cinfo[0])

        self.cinterrupt, cinfo = self.sinterrupt.accept()
        print ("Got a connection on the interrupt channel from " + cinfo[0])

        self.ccontrol.setblocking(False)
        self.cinterrupt.setblocking(False)
        thread = threading.Thread(target=self.recvthread, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()                                  # Start the execution


    def recvthread(self):
        print("Recv thread started")
        self.send_string(self.neutralPosition)
        while True:
            try:
                imsg = self.cinterrupt.recv(self.OutputReportSize + 1)
                response = None
                if imsg:
                    report = ord(imsg[1])
                    print("<= Receive %i bytes on %s: %s" % (len(imsg), hex(report), imsg.encode('hex')))
                    imsg = bytearray(imsg)
                    #TODO: handler map number -> function
                    if report == 0x01:
                        response = self.parseReportOne(imsg)
                    elif report == 0x10:
                        response = self.parseReportSixteen(imsg)
                    elif report == 0x11:
                        response = self.parseReportSeventeen(imsg)
                    else:
                        print("No handler for report %i" % report)
                if response:
                    self.send_string(response)

            except bluetooth.btcommon.BluetoothError:
                #Nothing received, thus nothing replied, take the opportunity to send unprompted updates
                if self.reportMode == 0x30:
                    self.send_string(self.update())
                elif self.reportMode == 0x31:
                    update = self.update()
                    update[1] = self.reportMode

                    nfc = bytearray([0] * 313)
                    nfc[0] = 0xff
                    calculator = crc8.crc8()
                    calculator.update(bytes(nfc[:-1]))
                    nfc[-1] = calculator.digest()

                    self.send_string(update + nfc)
                pass

            except Exception as e:
                print(type(e), e)
                pass
            time.sleep(self.wait)


    def update(self):
        response = bytearray([0] * (self.OutputReportSize + 1))
        response[0] = 0xa1
        response[1] = 0x30
        response[2] = self.nextSeq()
        response[3] = 0x8e # Battery | charge state
        #4, 5, 6 button stats
        response[4] = self.buttons[0]
        response[5] = self.buttons[1]
        response[6] = self.buttons[2]
        #7, 8, 9 left analog stick

        #10, 11, 12 right analog stick
        response[10] = 0x53
        response[11] = 0x88
        response[12] = 0x6e
        #13 vibration input report
        if self.imu:
            accel_x, accel_y, accel_z = [0] * 3, [0] * 3, [0] * 3
            gyro_1, gyro_2, gyro_3 = [0] * 3, [0] * 3, [0] * 3
            for i in range(0, 2):
                accel_x[i] = random.randint(0, 0xFF)
                accel_y[i] = random.randint(0xFF, 0x1000)
                accel_z[i] = random.randint(0x1000, 0xFFFF)
                gyro_1[i] = random.randint(0, 0xFF)
                gyro_2[i] = random.randint(0xFF, 0x1000)
                gyro_3[i] = random.randint(0x1000, 0xFFFF)

            #struct.pack_into("<18H", response, 14,
            #        accel_x[0], accel_y[0], accel_z[0],
            #        gyro_1[0], gyro_2[0], gyro_3[0],
            #        accel_x[1], accel_y[1], accel_z[1],
            #        gyro_1[1], gyro_2[1], gyro_3[1],
            #        accel_x[2], accel_y[2], accel_z[2],
            #        gyro_1[2], gyro_2[2], gyro_3[2]
            #)
            #85 01 f8 ff 94 f0 07 00 ec ff fd ff 80 01 f8 ff 92 f0 07 00 eb ff fc ff 80 01 f6 ff 90 f0 07 00 ec ff fa ff  <- sitting on desk
            # response[14:] = bytearray.fromhex('2d 01 0a 00 8a f0 / 08 00 ed ff 01 00 / 2b 01 0b 00 8b f0 / 07 00 ee ff 01 00 / 2c 01 09 00 8b f0 / 07 00 ed ff 00 00')
        return response

    def ack(self, imsg, data=bytearray()):
        response = bytearray([0] * (self.OutputReportSize + 1))
        response[0] = 0xa1
        response[1] = 0x21
        response[2] = self.nextSeq()
        response[3] = 0x8e # Battery | charge state
        #4, 5, 6 button stats
        response[4] = self.buttons[0]
        response[5] = self.buttons[1]
        response[6] = self.buttons[2]
        #7, 8, 9 left analog stick

        #10, 11, 12 right analog stick
        response[10] = 0x53
        response[11] = 0x88
        response[12] = 0x6e
        #13 vibration input report

        if (len(data)) == 0:
            response[14] = 0x80 # ack with no data
            response[15] = imsg[11] # subcommand ID
        else:
            response[14] = 0x80 + imsg[11]
            response[15] = imsg[11]
            response[16:16+len(data)] = data

        if imsg[11] == 0x21:
            calculator = crc8.crc8()
            calculator.update(bytes(data))
            response[-1] = calculator.digest()

        # No idea why, but 0x21 wants a response of 0xa0 (not 0xa1) and 0x04 wants a response of 0x83 (not 0x84)
        if imsg[11] == 0x21 or imsg[11] == 0x04:
            response[14] = response[14] - 1
        return response

    def nextSeq(self):
        self.sequence = (self.sequence + 1) % 0xFF
        return self.sequence

    def parseReportSeventeen(self, imsg):
        update = self.update()
        update[1] = self.reportMode

        nfc = bytearray([0] * 313)
        if imsg[11] == 0x01:
            if self.mcuConfig == 0x00:
                nfc[0] = 0x01
                nfc[4] = 0x04
                nfc[6] = 0x12
                nfc[7] = 0x01
            elif self.mcuConfig == 0x04:
                nfc[0] = 0x01
                nfc[4] = 0x04
                nfc[6] = 0x12
                nfc[7] = 0x04
        elif imsg[11] == 0x02 and imsg[12] == 0x04:
            nfc[0] = 0x2a
            nfc[2] = 0x05
            nfc[5] = 0x09
            nfc[6] = 0x31
        elif imsg[11] == 0x02 and imsg[12] == 0x01:
            nfc[0] = 0x2a
            nfc[2] = 0x05
            nfc[5] = 0x09
            nfc[6] = 0x31
            nfc[7] = 0x01
        else:
            nfc[0] = 0xff

        calculator = crc8.crc8()
        calculator.update(bytes(nfc[:-1]))
        nfc[-1] = calculator.digest()

        return update + nfc

    def parseReportSixteen(self, imsg): #Rumble
        return None

    def parseReportOne(self, imsg):
        subcommand = imsg[11]
        response = None
        if subcommand == 0x02:
            print("Request device info")
            response = self.ack(imsg, bytearray.fromhex('03 86 02 02 E0 76 D0 DD AA 99 01 01'))
        elif subcommand == 0x03:
            self.reportMode = imsg[12]
            print("Set input report mode %02x" % self.reportMode)
            response = self.ack(imsg)
        elif subcommand == 0x04:
            print("trigger buttons elapsed time")
            response = bytearray.fromhex('a1 21 a2 6e 00 00 00 00 00 00 72 78 6a 0b 83 04 00 00 ff ff 00 00 ff ff ff ff ff ff 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00')
            #response = self.ack(imsg, bytearray.fromhex('00 00 b8 31'))
        elif subcommand == 0x08:
            print("set shipment")
            response = self.ack(imsg)
        elif subcommand == 0x10: #SPI flash read
            address = struct.unpack('<H', imsg[12:14])[0]
            print("SPI flash read address %04x" % address)
            if address == 0x6000:
                response = self.ack(imsg, bytearray.fromhex('00 60 00 00 10 00 00 58 43 57 31 30 30 35 33 32 36 34 37 32 34'))
            elif address == 0x6020:
                response = self.ack(imsg, bytearray.fromhex('20 60 00 00 18 43 ff cd 00 77 01 00 40 00 40 00 40 fd ff ff ff fc ff 3b 34 3b 34 3b 34'))
            elif address == 0x603d:
                response = self.ack(imsg, bytearray.fromhex('3d 60 00 00 19 ff ff ff ff ff ff ff ff ff 6b 38 6b 3d 05 4b 26 b5 46 ff 7F FF 00 1e 0a 0a'))
            elif address == 0x6080:
                response = self.ack(imsg, bytearray.fromhex('80 60 00 00 18 5e 01 00 00 0f f0 19 d0 4c ae 40 e1 ee e2 2e ee e2 2e b4 4a ab 96 64 49'))
            elif address == 0x6098:
                response = self.ack(imsg, bytearray.fromhex('98 60 00 00 12 19 d0 4c ae 40 e1 ee e2 2e ee e2 2e b4 4a ab 96 64 49'))
            elif address == 0x8010:
                response = self.ack(imsg, bytearray.fromhex('10 80 00 00 18 ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff ff'))
            else:
                print("No response data for %02x" % address)
        elif subcommand == 0x21: #set MCU config
            print("Set MCU config %02x" % imsg[12])
            self.mcuConfig = imsg[14]
            response = self.ack(imsg, bytearray.fromhex('01 00 00 00 04 00 12 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00'))
        elif subcommand == 0x22: #set MCU state
            print("Set MCU state %s" % imsg[12])
            response = self.ack(imsg)
        elif subcommand == 0x30: #set player lights
            print("Set player lights")
            response = self.ack(imsg)
        elif subcommand == 0x40: # IMU (6 axis sensor)
            self.imu = (imsg[12] == 0x01)
            print("IMU sensor %s" % self.imu)
            response = self.ack(imsg)
        elif subcommand == 0x48: # Vibration on/off
            print("Vibration %s" % (imsg[12] == 1))
            response = self.ack(imsg)
        else:
            print("No response configured for %i" % subcommand)
        return response

    #send a string to the bluetooth host machine
    def send_string(self, message):
        message = bytes(message)
        print("=>Sending %s" % message.encode('hex'))
        self.cinterrupt.send(message)

    def change_button_state(self, keys):
        # Translate array of keyboard codes (https://gist.github.com/MightyPork/6da26e382a7ad91b5496ee55fdc73db2)
        # to 0x30 report style update bytes
        # 04 00 00 00 00 00
        buttons = [0] * 3
        for k in keys:
            if (k == 0x04): # A
                buttons[0] = buttons[0] | 0x08
            if (k == 0x05): # B
                buttons[0] = buttons[0] | 0x04

            if (k == 0x0F): # L
                buttons[0] = buttons[0] | 0x40
        self.buttons = buttons

#define a dbus service that emulates a bluetooth keyboard
#this will enable different clients to connect to and use the service
class BTKbService(dbus.service.Object):
    def __init__(self):
        print("Setting up service")

        #set up as a dbus service
        bus_name=dbus.service.BusName("org.yaptb.btkbservice",bus=dbus.SystemBus())
        dbus.service.Object.__init__(self,bus_name,"/org/yaptb/btkbservice")

        #create and setup our device
        self.device= BTKbDevice();

        #start listening for connections
        self.device.listen();


    @dbus.service.method('org.yaptb.btkbservice', in_signature='yay')
    def send_keys(self, modifier_byte, keys):
        self.device.change_button_state([c for c in keys])


#main routine
if __name__ == "__main__":
    # we an only run as root
    if not os.geteuid() == 0:
        sys.exit("Only root can run this script")

    gobject.threads_init()

    DBusGMainLoop(set_as_default=True)
    myservice = BTKbService();
    gtk.main()

